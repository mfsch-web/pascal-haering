function title(p)
    local t = pandoc.read(io.lines("content"..p..".md", "*a")()).meta["title"]
    if t == nil then
        t = pandoc.MetaString("")
    end
    return t
end

function Meta(m)

  -- skip everything for index file
  if pandoc.utils.stringify(m.url) == "/index" then
      return m
  end

  -- add images if gallery is enabled
  if m.gallery ~= nil then
      local gallery = pandoc.utils.stringify(m.gallery.folder)
      local images = {}
      for fn in io.popen("ls images/"..gallery.."/*.jpg"):lines() do
          local id = fn:gsub("^images/"..gallery.."/([^ ]+).*", "%1")
          local caption = fn:gsub("^images/"..gallery.."/"..id.." ", ""):gsub("%.jpg$", "")
          images[#images + 1] = pandoc.MetaMap({ id=id, caption=caption })
      end
      table.sort(images, function(a,b) return pandoc.utils.stringify(a.id) > pandoc.utils.stringify(b.id) end)
      m["gallery-images"] = pandoc.MetaList(images)
  end

  -- read nav order, title, and description from index file
  local index = pandoc.read(io.lines("content/index.md", "*a")()).meta
  m["site-title"] = index["title"]
  if m.description == nil then
      m.description = index.description
  end
  if m.keywords == nil then
      m.keywords = index.keywords
  end
  local order = index["nav-order"]
  for i,v in ipairs(order) do
      order[i] = "/"..pandoc.utils.stringify(v)
  end
  function iorder(s)
      for i,v in ipairs(order) do
          if v == s then; return i; end
      end
      return #order + 1
  end
  function sortorder(a, b) -- like <
    local ia, ib = iorder(a), iorder(b)
    if ia == ib then -- both not found
      return a < b
    else
      return ia < ib
    end
  end

  -- parse url into segments
  local url = pandoc.utils.stringify(m["url"])
  local lang = url:gsub("/(..)/.*", "%1")
  local page = url:gsub("/..(/[^/]+).*", "%1")
  m["lang-"..lang] = pandoc.MetaBool(true)

  -- build list of entries in main menu
  local pages = {}
  for p in io.popen("ls content/"..lang.."/*.md"):lines() do
      pages[#pages + 1] = p:sub(11):gsub("%.md", "")
  end
  table.sort(pages, sortorder)

  -- build list of entries in main menu
  subpages = {}
  local subdir = "content/"..lang.."/"..page
  for p in io.popen("[ -d '"..subdir.."' ] && ls "..subdir.."/*.md"):lines() do
      subpages[#subpages + 1] = p:gsub("content/../(.*)%.md", "%1")
  end
  table.sort(subpages, sortorder)

  -- add menu to metadata
  for i,p in ipairs(pages) do
      pages[i] = {
          name=title("/"..lang..p),
          url=pandoc.MetaString("/"..lang..p),
          current=pandoc.MetaBool(p == page),
      }
  end
  m.pages = pandoc.MetaList(pages)

  -- create pandoc metadata for entries
  for i,p in ipairs(subpages) do
      subpages[i] = {
          name=title("/"..lang..p),
          url=pandoc.MetaString("/"..lang..p),
          current=pandoc.MetaBool("/"..lang..p == url),
      }
  end
  m.subpages = pandoc.MetaList(subpages)

  return m
end

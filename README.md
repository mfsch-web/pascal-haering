# Website Pascal Häring

This is the repository of the website <https://www.pascalhaering.com>.

## Content

The website content is stored in the folder `content` with one file per page.
The content is formatted with [Markdown](https://en.wikipedia.org/wiki/Markdown) and is transformed to HTML with [Pandoc](https://pandoc.org/).
There are many guides and cheat sheets available for Markdown (e.g. [this one](https://www.markdownguide.org/)), but the authoritative source for the website is the [Pandoc manual](https://pandoc.org/MANUAL.html#pandocs-markdown).
Pandoc is currently run with the options `auto_identifiers` and `implicit_figures` deactivated.
Note that the way Markdown files are shown here on GitLab is based on [GitLab’s Markdown formatting rules](https://docs.gitlab.com/ee/user/markdown.html) so it is possible that some things look different here than when they are published on the website.
Another useful thing to know is that the Markdown content can contain HTML tags that are simply passed on to the HTML output.

The page tree is based on the files in `content/de` and `content/en` as well as their (direct) sub-folders.
To create a new page, simply add a file in one of these folders with `PAGE.md` as the name, after which it will be published as `/LANG/PAGE`.
To create sub-pages, create a folder `PAGE` and add a file `SUBPAGE.md` inside, after which it will be published as `/LANG/PAGE/SUBPAGE`.
If a subpage is in a folder `LANG/PAGE/SUBPAGE.md` for which there exists no file `LANG/PAGE.md`, the HTML file will be created correctly but there is no way to get to the page through the navigation.

## Metadata

The content files have a block of [YAML]() metadata at the top.
As described in the [Pandoc manual](https://pandoc.org/MANUAL.html#extension-yaml_metadata_block), this is used to provide some additional data to the HTML templates.
Below are the properties that are “understood” by the template:

- `title` (required): the title of the page that is used in the menu and in the HTML `<title>` tag (shown as title of the browser window/tab) in combination with the `title` of `index.md`
- `description`: used in the HTML `<meta name="description">` tag that is sometimes shown in search results; if empty, description from `index.md` is used
- `keywords`: used in the HTML `<meta name="keywords">` tag that might be considered in search results; if empty, keywords from `index.md` are used
- `colors`: override the default theme colors for the page, in the format e.g. `FF0000` for red (no `#` sign, capitalization does not matter)
  - `background`: background color around the content blocks
  - `blocks`: background color inside the content blocks
  - `text`: text color of the main content and the navigation
  - `links`: text color of links inside the content (set to `background` color if not specified)
- `other-language`: path of the site that is shown when the language is switched, e.g. `willkommen` or `acts/juggling`, without leading/trailing slash and without `de`/`en` (if empty, the welcome page of the other language is linked)
- `gallery`: add a picture gallery at the end of the page
  - `folder` (required): the name of a folder inside `images`, the pictures of which are shown in the gallery; the pictures should be named `ID Caption of the image.jpg`, where the ID part is used for the order (reversed) and the rest of the name after the first space is used as caption for the image; **note that the extension should always be lowercase `jpg`**
  - `caption`: text that will be shown in a paragraph underneath the gallery

## `index.md`

The file `content/index.md` is used for the home page.
The meaning of the meta tags is a bit special since other pages use this data as well.

- `title`: the title here is also used as part of the `<title>` tag of all the other pages, separated by `::`
- `home`: should always be set to `true`
- `description`: also used as fallback for other pages
- `keywords`: also used as fallback for other pages
- `nav-order`: a list of page paths (similar to `other-language`, without language and without leading/trailing slashes), the relative order of which defines the order in the main and sub-page navigation; pages that are not found in this list are placed in alphabetical order after the listed pages

## Images

Images are usually placed directly inside the `images` folder, unless they are part of a gallery (see above), in which case they are placed in a subfolder.
File names that end in `-full.jpg` or `-small.jpg` should be avoided, as they interfere with the way galleries are implemented.

## Fixes & Improvements

- proper csp for iframes
- add 404 for languages
- header: fix based on h5bp (meta for theme etc.)

### Outdated Links

<https://www.migrosmagazin.ch/menschen/portraet/artikel/zirkusartist-haering-und-sein-roue-cyr>
<https://www.sn-herne.de/index.php?cmd=article&aid=5314>
<https://www.adelaidenow.com.au/ipad/acrobats-survive-rough-and-tumble/story-fn6t2xlc-1226063793044>
<https://quakestudies.canterbury.ac.nz/store/download/part/230630>

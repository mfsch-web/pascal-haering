# settings
SITE = public
TOOLS = tools
PANDOC_VERSION = 2.11.2
SASS_VERSION = 1.29.0
JQUERY_VERSION = 3.5.1
FANCYBOX_VERSION = 3.5.7

.DEFAULT_GOAL := site
.PHONY: site clean
.INTERMEDIATE: $(TOOLS)/fancybox.tar.gz
.PRECIOUS: scripts/jquery-% scripts/jquery.fancybox.min.js styles/jquery.fancybox.min.css

pandoc = $(TOOLS)/pandoc-$(PANDOC_VERSION)/bin/pandoc
pandoc_cmd = $(pandoc) --from=markdown-auto_identifiers-implicit_figures --wrap=preserve
sass = $(TOOLS)/dart-sass/sass

content = /index \
      $(wildcard content/de/*.md) \
      $(wildcard content/de/*/*.md) \
      $(wildcard content/en/*.md) \
      $(wildcard content/en/*/*.md)
#content = /en/pictures
content := $(patsubst content/%.md,/%,$(content))
static := $(patsubst static/%,/%,$(wildcard static/*)) \
	 $(patsubst static/%,/%,$(wildcard static/*/*))
static := $(filter-out $(patsubst %/,%,$(dir $(static))),$(static))

images := $(wildcard images/*)
galleries := $(filter-out .,$(sort $(patsubst %/,%,$(dir $(wildcard images/*/*)))))
images := $(patsubst images/%,/img/%,$(filter-out $(galleries),$(images)))
$(info galleries: $(image_dirs))
gallery_images = $(shell ls $(galleries)/* | cut -d' ' -f1)
gallery_full = $(patsubst images/%,/img/%-full.jpg,$(gallery_images))
gallery_small = $(patsubst images/%,/img/%-small.jpg,$(gallery_images))

$(info content: $(content))
$(info galleries: $(galleries))

paths = $(addsuffix .html,$(content)) \
    /css/main.css \
    /css/jquery.fancybox.min.css \
    /js/jquery.min.js \
    /js/jquery.fancybox.min.js \
    $(gallery_full) \
    $(gallery_small) \
    $(images) \
    $(static)

site: $(addprefix $(SITE),$(paths))

clean:
	rm -rf $(SITE) $(TOOLS)

$(pandoc):
	@mkdir -p $(TOOLS)
	wget -O $(TOOLS)/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
	tar -xzf $(TOOLS)/pandoc.tar.gz -C $(TOOLS)

$(sass):
	@mkdir -p $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/$(SASS_VERSION)/dart-sass-$(SASS_VERSION)-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

scripts/jquery-%:
	wget -O $@ https://code.jquery.com/jquery-$*

$(TOOLS)/fancybox.tar.gz:
	wget -O $@ https://github.com/fancyapps/fancybox/archive/v3.5.7.tar.gz

styles/jquery.fancybox.min.css scripts/jquery.fancybox.min.js: | $(TOOLS)/fancybox.tar.gz
	tar -xOf $(TOOLS)/fancybox.tar.gz fancybox-$(FANCYBOX_VERSION)/dist/$(notdir $@) > $@

$(addprefix $(SITE),$(static)): $(SITE)/%: static/%
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/css/%: styles/%
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/img/%: images/%
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/js/jquery.min.js: scripts/jquery-$(JQUERY_VERSION).min.js
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/js/%: scripts/%
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/css/%.css: styles/%.scss $(wildcard styles/_*.scss) | $(sass)
	@mkdir -p $(@D)
	$(sass) $< $@

$(SITE)/%.html: content/%.md $(patsubst /%,content/%.md,$(content)) $(wildcard templates/*) | $(pandoc)

	@mkdir -p $(@D)
	$(pandoc_cmd) --template=templates/default.html \
	    --metadata=url:/$* \
	    --lua-filter=templates/add-navigation.lua \
	    --output=$@ $<

.SECONDEXPANSION:

$(SITE)/img/%-full.jpg: $$(shell ls images/%\ * | head -n1 | sed 's/ /\\ /g')
	@mkdir -p $(@D)
	cp "$(subst ",\",$<)" $@

$(SITE)/img/%-small.jpg: $$(shell ls images/%\ * | head -n1 | sed 's/ /\\ /g')
	@mkdir -p $(@D)
	convert "$(subst ",\",$<)" -scale 130x130^ -gravity center -extent 130x130 +profile "*" -sharpen 0x1 $@

---
title: Aktuell
other-language: news
colors:
  background: 2c5299
  blocks: ebebeb
---


Agenda
======

[2023]{style="font-size: 1.17em;"}

24\. April 2023, 20:00 - _Falling Down the Rabbit Hole_ im Gundeldinger Feld, Basel. Siehe [www.cirqueducercle.com](http://www.cirqueducercle.com)

25\. April 2023, 20:00 - _Falling Down the Rabbit Hole_ im Gundeldinger Feld, Basel. Siehe [www.cirqueducercle.com](http://www.cirqueducercle.com)

26\. April 2023, 14:00 - _Falling Down the Rabbit Hole_ im Gundeldinger Feld, Basel. Siehe [www.cirqueducercle.com](http://www.cirqueducercle.com)

23\. Juni 2023 - Gala in Bülach (CH)

2023 (das ganze Jahr) - UK-Tournee mit _Cirque - The Greatest Show_, Siehe [https://www.entertainers.co.uk/show/cirque-the-greatest-show](https://www.entertainers.co.uk/show/cirque-the-greatest-show)


Aktuelle News auf Facebook
==========================

<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fhaeringpascal&tabs=timeline&width=500&height=5000&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId" width="500" height="5000" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>

---
title: Unterricht
other-language: teaching
colors:
  background: b50909
  blocks: 45ffbb
---

Unterricht
==========

### Bristol Cyr Wheel Weekend

Ungefäht alle zwei Monate organisiere ich ein Cyr Wheel Wochenende für
Anfänger und Fortgeschrittene in Bristol. Bitte kontaktiere mich bei
Interesse.

### Workshops / Privatunterricht

Ich habe 20 Jahre Erfahrung als Kursleiter von Zirkus-, Ski- und
Unihockeykursen für Kinder, Jugendliche und Erwachsene. Wenn Sie
Interesse an Workshops oder Privatunterricht haben, kontaktieren Sie
mich bitte.

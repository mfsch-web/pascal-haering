---
title: Cirque du Cercle
other-language: artistic/cirque-du-cercle
colors:
  background: 7d0000
  blocks: ffffff
  text: 000000
---

Cirque du Cercle
================

Cirque du Cercle ist ein gemeinnütziger Verein mit Sitz in Sissach BL,
welcher zeitgenössische Zirkusartisten mit einem Bezug zur Region
Nordwestschweiz unterstützt und fördert. Er lanciert und produziert
insbesondere auch selbst Projekte und Produktionen im Genre Nouveau
Cirque. Eine eigene Webseite ist in der Entstehung, aber bis es soweit
ist, sind hier schon mal ein paar Informationen zu finden.

Mehr Informationen zu Cirque du Cercle und dem neuen Stück "Falling down the Rabbit Hole" gibt's unter [www.cirqueducercle.com](http://www.cirqueducercle.com). Vom 24. - 26. April 2023 spielen wir das Stück im Gundeldinger Feld in Basel.

<!--
### Falling Down the Rabbit Hole

Zwei ArtistInnen finden sich unversehens in einer surrealen Welt voller Absurditäten, Illusionen und Paradoxa wieder. Wie Alice im Wunderland sind sie fasziniert von den eigenartigen Vor-kommnissen um sie herum und verirren sich immer tiefer in der scheinbar irrealen Welt. Falling Down the Rabbit Hole ist ein verspieltes, poetisches und absurdes Stück zeitgenössischer Zirkus und eine Metapher dafür, wie einfach es ist, sich in den unendlichen, manchmal surrealen Tiefen des In-ternets und anderer Informationsquellen zu verlieren und wie schnell sich die Grenzen zwischen Realität und Fiktion verwischen.

Nie war es einfacher an Informationen zu kommen, und gleichzeitig war es nie schwieriger, sich im schier unendlichen Universum von Wissen, Daten, Unterhaltung und Ablenkungen zurechtzufinden. Im Internet hat es heute rund 1.2 Milliarden Webseiten. Dabei das Richtige, Wichtige, Unnütze und 
Falsche voneinander zu unterscheiden, ist manchmal eine kaum zu meisternde Herausforderung.

In Lewis Carrolls Geschichte aus dem Jahr 1865 fällt Alice in den Kaninchenbau, weil sie einem spre-chenden weissen Kaninchen mit einer Taschenuhr folgt, und landet in einer Welt voller Wunder. Heu-te ist der Ausdruck "falling down the rabbit hole" in der englischsprachigen Welt zum Synonym dafür geworden, sich in einem Strudel von Ablenkung und (Fehl-)Informationen zu verlieren. Oder, wie Ka-thryn Schulz in einem Artikel im The New Yorker schreibt: «Wenn wir heute sagen, dass wir in den Ka-ninchenbau gefallen sind, […] meinen wir damit, dass wir uns für etwas so sehr interessieren, dass es uns ablenkt - in der Regel zufällig und in einem Ausmass, welches das betreffende Thema nicht ver-dient.»

Und weiter: «Als Metapher für unser Online-Verhalten hat das Kaninchenloch einen Vorteil, der ande-ren fiktionalen Portalen fehlt: Es vermittelt ein Gefühl von Zeit, die man unterwegs verbringt. In der Originalgeschichte fällt Alice für eine ganze Weile - lange genug, um die Umgebung zu erkunden, sich etwas zu essen aus einem vorbeiziehenden Regal zu holen, irrtümlich über andere Teile der Welt zu spekulieren, in eine Träumerei über Katzen abzudriften und fast einzuschlafen. Das klingt ganz nach uns im Internet. Bei der heutigen Verwendung von ‘rabbit hole’ sind wir nicht mehr unbedingt auf dem Weg in ein Wunderland. Wir befinden uns einfach in einem langen, aufmerksamkeitsgesteuerten freien Fall, ohne klares Ziel und mit allerlei seltsamen Dingen, die an uns vorbeirauschen.»

In Falling Down the Rabbit Hole fallen die beiden ArtistInnen in ihren ganz eigenen Kaninchenbau, eine absurde Welt aus Informations-Illusionen, Absurditäten und Paradoxa. „Rabbit Holes“ können tief sein und deren Ausstieg schwer zu finden. Sie können einen auf einen Pfad des Wissens und der Entde-ckung führen, aber auch zu Lügen, Fehlinformationen und Täuschung. Sie sind gleichzeitig ein Para-dies für WissenschaftsliebhaberInnen und VerschwörungstheoretikerInnen, und es ist oft schwierig zu erkennen, wo die Wahrheit liegt. Wer lügt uns an und wer nicht? Welches „rabbit hole“ führt uns zur Realität, und welches ist voller Täuschungen? Können wir dem Ungewissen vertrauen? Das Stück ist inspiriert von Überlegungen der ArtistInnen zu Themen wie Informationsüberflutung, Verschwö-rungstheorien, der Grenze zwischen Vernunft und Wahnsinn und der Frage, wie man in all dem Chaos einen moralischen Kompass findet kann. 
 
Gemeinsam mit der Regisseurin Delia Dahinden entwickeln der Schweizer Artist Pascal Häring und die Walisische Artistin Esther Fuge ein ca. 60-minütiges Bühnenstück im Stil des Neuen Zirkus. Durch Improvisationen und prozessorientiertes Arbeiten sind sie gerade daran, sich in den Untiefen des Ka-ninchenbaus eine fantastische Welt zu erfinden. Es wird eine Welt geprägt von Ringen und Stoffen, von Illusionen und Absurditäten, eine Welt, in der die beiden ArtistInnen von der vermeintlichen Wahrheit ausgetrickst werden. Anspielungen auf Alice im Wunderland erinnern uns daran, dass Dinge nicht immer so sind, wie sie scheinen, und Menschen nicht das, was sie vorgeben zu sein. Wir sind gespannt, wie sich die Reise durch diese Welt in den nächsten Monaten weiter entwickeln wird. Bis zur Premiere im August wissen wir mehr.


Wir bedanken uns ganz herzlich bei den folgenden Förderinnen für die Unterstützung:

![FA_BSBL_TUT_rgb_screen-2.png](/img/FA_BSBL_TUT_rgb_screen-2.png){width="400"}

![WAI_ACW_WG_LOT.jpg](/img/WAI_ACW_WG_LOT.png){width="580"}

![MOB_d_RGB_rot.svg](/img/MOB_d_RGB_rot.svg){width="250"}&nbsp;&nbsp;![EGS_Schriftzug_2_schwarz.jpg](/img/EGS_Schriftzug_2_schwarz.jpg){width="200"}

&nbsp;&nbsp;![Logo_Kultur_Bildung_RGB.jpg](/img/Logo_Kultur_Bildung_RGB.jpg){width="200"}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![10653-logo-zirkusquartier-zurich.png](/img/10653-logo-zirkusquartier-zurich.png){width="150"}

![Wallach.png](/img/Wallach.png){width="200"}&nbsp;&nbsp;&nbsp;&nbsp;![Logo_FGE_MK_rgb_72dpi_DE.png](/img/Logo_FGE_MK_rgb_72dpi_DE.png){width="250"}



#### Recherche

Unter dem Arbeitstitel _(Wo)man_ haben wir im Oktober 2020, Mai und September 2021 drei artistische Residenzen mit der walisischen Artistin Esther Fuge und dem sissacher Artisten Pascal Häring durchgeführt, um Themen, Ideen und künstlerische „Sprache" für eine verstärkte Zusammenarbeit zu entwickeln und testen. Ausgangsthema der Recherche waren Fragestellungen zu Rollen-, Status- und Machtunterschieden zwischen den Geschlechtern: Wie können ungesunde Geschlechterdynamiken auf der Bühne thematisiert und entlarvt werden? Der Prozess hat uns zu einer Reflektion über das Lügen und schliesslich zum unten beschriebenen Thema "Facts and Fake News" geführt, welches uns aufgrund seiner Aktualität sofort faszinierte.

Während diesen Recherchen haben wir unter anderem mit der walisischen Regisseurin Gwen Scott und den schweizer Regisseurinnen Delia Dahinden und Philipp Boë zusammengearbeitet.

Wir bedanken und herzlich bei unseren Partnern und Unterstützern des
Recherche-Projekts:

![farbig_d.gif](/img/5e435a92abf9f7e27413fe656de0a94b_f154.gif){width="200" height="75"}

![Logo_Kultur_Bildung_RGB.jpg](/img/Logo_Kultur_Bildung_RGB.jpg){width="150" height="64"}

**Einwohnergemeinde Sissach**

**Bürgergemeinde Sissach**
-->

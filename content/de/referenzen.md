---
title: Referenzen
other-language: references
colors:
  background: 155452
  blocks: ccb6b6
---

Kunden und Zuschauer Feedback
=============================

[Referenzen](#referenzen) – [Auszeichnungen](#preise) – [In der
Presse](#presse) – [Mitgliedschaften](#mitgliedschaften)

**Sensirion AG**\
*Hallo Pascal\
Vielen Dank an dich – es war wirklich ein sehr schöner und
stimmungsvoller Auftritt, der uns allen lange in Erinnerung bleiben
wird.\
Liebe Grüsse\
Pamela*

**Art on Stage, Biel**\
*Hoi Pascal\
Herzlichen Dank für Deine Worte und die geniale Umsetzung des Songs. Ich
bin sehr glücklich, dass ich Dich kennen lernen durfte.\
Deine künstlerische Umsetzung entspricht genau meinem Geschmack und
bestätigt meine Anfrage an Dich.\
Auch die Zuschauer waren sehr berührt von Deinem Auftritt.\
Die Show war ein voller Erfolg. Die Feedbacks überwältigend.\
Dani*

**Monika Gerhardt, Zuschauerin beim Varieté Et Cetera**\
*Hallo,\
ich war heute in der Sonderveranstaltung im Variete et cetera und Ihre
Nummer war mehr als super!\
Vor allem in Verbindung mit der Musik fand ich sie sozusagen
"traumhaft".\
Liebe Grüße und weiterhin viel Erfolg,\
Monika Gerhardt*

**Andreas Wølner-Hanssen, Dozent DSBG Universität Basel**\
*Hallo Pascal\
Ich schaue gerade die grössten Schweizer Talente und bin begeistert von
Deiner Performance. Das ist so unglaublich schwer, was Du da mit einer
Leichtigkeit uns gezeigt hast. Ich bin unter anderem Dozent am
Departement für Sport, Bewegung und Gesundheit der Universität Basel und
unterrichte dort das Fach Bewegungskünste. Das ist also genau Dein
Gebiet!\
Bewegte Grüsse\
Andreas*

Referenzen {#referenzen}
========================

### 2020

Royal Shakespeare Company, Stratford-upon-Avon UK\
CARO, Efteling Park / Imagine Shows, Tilburg NL

### 2019

CARO, Efteling Park / Imagine Shows, Tilburg NL\
Eisenbahner-Baugenossenschaft beider Basel, Basel CH\
IN COLOUR, Swing Circus, Salisbury International Festival, Salisbury UK\
LED Cyr Wheel, Remy Martin / Smoking Hot Productions, Lagos Nigeria\
Kantonsschule Wil, Maturaball, Wil SG, CH\
Bucher Municipal, Sommerfest, Niederweningen CH\
Shopville, Zurich UK\
European Juggling Convention, Gala Show, Newark UK\
Glenmark, Zürich CH\
Smoking Hot Productions, Brighton UK\
Autorsport Schweiz, Bern CH\
SBB Immobilien, Pfäffikon CH\
AECC University College, Bournemouth UK\
Kunststoff Schwanden, Schwanden CH

### 2018

Flying Colours, Lotherton Hall, Leeds UK\
Organised Kaos, Nandos, Newport UK\
Mayor of Barnett Gala Ball, UK\
Al Kout Mall, Kuwait\
Bristol Swing Festival 2018, Bristol UK\
Evonik, Rheinfelden DE\
Sportamt Zug, Zuger Sportnacht 2018, Zug CH\
Tattoo & Art Show, Lörrach DE\
Photon Cyr Wheel, Smoking Hot Productions, London UK\
Swing Circus, Beautiful Days Festival, Devon UK\
Swing Circus, R&D and Premiere von IN COLOUR, Circomedia Bristol UK\
Salto Sociale, Benefiz Gala, Lörrach DE

### 2017

Christmas Circus Lörrach, Lörrach DE\
LR Health & Beauty Systems, Zürich CH\
Tumbellina, Mall of Asia, Manila PHL\
Circus City Festival, Swing Circus, Bristol UK\
ÔlaC Festival 2017, Milvignes / Neuchâtel CH\
Insight Ensemble goes Outer-Space!, Bristol UK\
Tilburg Fietst Festival (Cycling Festival), Tilburg NL\
Schule Bethlehemacker, Bern CH\
Valley Fest, Chew Valley Lake UK\
Bristol Harbour Festival, Circue Bijou, Bristol UK\
Circus Fantastic, Bristol UK\
Pfizer, Surrey UK\
Coppélia, Feathers of Daedalus, Jackson's Lane, London UK\
Bristol Swing Dance Festival, Bristol UK\
Impro Cirque, Monthey CH\
Schneeshow Arosa, Arosa CH\
Basler School Dance Award, Lausen CH

### 2016

Best Parties Ever, Malvern UK\
UNIA, Schaffhausen CH\
Performers Without Borders, Fundraiser, Bristol UK\
BACA Workwear and Safety, Silverstone UK\
Heathrow Charity Ball, Heathrow UK\
Tumbellina International, UK\
Ziefener Schauturnen, Ziefen CH\
British Acrobatics Festival, Oxford UK\
Jugendzirkus Prattelino, Pratteln CH\
Clinam (European Foundation for Clinical Nanomedicine), Basel CH\
Glastonbury Festival, Circus Big Top, Glastonbury UK\
Invisible Circus, Bristol UK\
Kinder-Camps, Akrobatik- und Jonglier-Unterricht, Sissach CH\
Zirkuswerkstatt Liestal, Zirkus-Unterricht, Liestal CH\
Planet Bowie Cabaret, Albany Circus Centre, Bristol UK\
Die grössten Schweizer Talente, TV-Show, SRF 1, Zurich CH

### 2015

Wie-n en Stärn, wo am Himmel stoht; Aarau CH\
Art on Stage, Biel-Bienne CH\
Camvention Gala Show, Cambridge GB\
Barnum Musical, Cameron Mackintosh, GB Tournée\
Sensirion AG, Firmen-Gala, Zürich CH

### 2014

Noël en Cirque, Valence d'Agen F\
Barnum Musical, Cameron Mackintosh, GB Tournée\
- Curve Theatre, Leicester\
- Bristol Hippodrome\
- Cliffs Pavilion, Southend\
- Wycombe Swan\
- New Wimbledon Theatre\
- Hull New Theatre\
- Edinburgh Playhouse\
- Manchester Palace\
- His Majesty's Theatre, Aberdeen\
- Bradford Alhambra\
Boulder Juggling Festival, Gala, Boulder US-CO\
Dräger, Jubiläums-Gala 125 Jahre Dräger, Lübeck D\
Greentop Cabaret, Sheffield GB\
Circus North Incubator, Greentop Circus, Sheffield GB

### 2013

Festival des Artistes de Cirque, Saint-Paul-lès-Dax F\
Geburtstagsfest, 70 Jahre Ueli Pfister, Liestal CH\
M/S Color Fantasy, Color Line, Fähre zwischen Kiel (D) und Oslo (N) \
Cirque Bouffon, Nandou, Köln D\
Varieté Et Cetera, Weggezapped, Bochum D\
Tasmanian Circus Festival, Golconda (TAS) AUS

### 2012

Swiss Christmas, Diamond Illusion, Zürich Oerlikon CH\
Schule für Körper- und Mehrfachbehinderte, Zürich CH\
FAQoustic Swiss Talent Tour, Rheinfelden (Baden) D\
Circus Gasser Olympia, Gala Show für Ernst Frey AG, Kaiseraugst CH\
Schweizerische Jonglier Convention, Wetzikon CH\
Europäische Jonglier Convention, Lublin PL\
Monday Night Magic @ The Dark Room, Christchurch NZ\
TV2 KidsFest, The Flying Kiwi Circus Show, Christchurch NZ\
Christchurch Circus Centre Open Night, Christchurch NZ\
Cabaret Saveloy, The Ice Cream Factory, Brisbane AUS\
The Woohoo Revue concert, Wollongong AUS\
Private function, Wollongong AUS\
Circus Aotearoa, Neuseeland (3-Monate Saison)

### 2011

Melbourne Fringe Festival, "This old city", AUS\
Melbourne Jonglier Convention, AUS\
Melbourne Circus Festival, AUS\
National Institute of Circus Arts (NICA), "Lost Ground",
Melbourne AUS\
Variety Collective, Melbourne AUS\
Club Voltaire, "Month of Sundays", Melbourne AUS\
Cabaret Vertigo, Melbourne AUS\
Erdbeben Sozial- und Auffangzentren, Christchurch NZ

### 2003 – 2010

Circus under the stars, Christchurch NZ\
CPIT school of performing arts, "Finale", Christchurch NZ\
Zirkus Stern, Sempach CH (3 Saisons)\
Gewerbeausstellung, Gelterkinden CH\
Uniball, Basel CH\
Arosa Tourismus Schneesport Show, Arosa CH\
Zirkus Prattelino, Pratteln CH\
Diverse Privatanlässe, CH

Auszeichnungen {#preise}
========================

Boulder Circus Award 2014

In der Presse {#presse}
=======================

Hier einige Links zu Zeitungsartikeln, Radio-Interviews and
Video-Berichterstattungen, in denen über Pascal Häring berichtet wurde.

Mitarbeitermagazin der ING-Diba, Rubrik "Ich sag's euch"\
März 2017\
<https://du.ing-diba.de/archiv/2017_01/ich_sag_s_euch.html>

Aroser Zeitung\
24. März 2017\
[Artikel Aroser Zeitung](/Article-AroserZeitung-Feb2017.pdf)

Regio aktuell\
May 2016\
<https://www.regio-info.ch/images/stories/regio-artikel/2016/05-16/pdf/ausgewandert.pdf>

Schweizer Illustrierte Online\
27. Februar 2016\
<https://www.schweizer-illustrierte.ch/stars/schweiz/dgst-2016-pascal-haering-vor-jury-jonny-fischer>

Beruf + Berufung (Bund, Berner Zeitung, Tamedia-Newsnet, Ostschweiz am
Sonntag, ostjob.ch, beruf-berufung.ch), Schweiz\
28. November 2015\
<https://blog.derbund.ch/berufung/index.php/34920/wenn-man-wenig-plant-bleibt-mehr-zeit-fuer-schoene-ueberraschungen/>

Migros Magazin, Schweiz\
2. November 2015\
<https://www.migrosmagazin.ch/menschen/portraet/artikel/zirkusartist-haering-und-sein-roue-cyr>

Radio SRF 1: "Die fünfte Schweiz", Schweiz\
25. Oktober 2015\
<https://www.srf.ch/sendungen/die-fuenfte-schweiz/pascal-haering-mit-dem-zirkusrad-um-die-welt>

Bay TV, Liverpool, Grossbritannien (Englisch)\
20. Mai 2015\
<https://youtu.be/WSqJ8i-0aWM?t=7m6s>

Sonntagsnachrichten, Herne, Deutschland\
Februar 2013\
<https://www.sn-herne.de/index.php?cmd=article&aid=5314>

THERAPIE - Das Lifestylemagazin, Essen, Deutschland\
Februar 2013\
<https://www.therapie-online.de/home/artikel/view/58/weggezapped/>

NZZ, Zürich, Schweiz\
27. November 2012\
<https://www.nzz.ch/zuerich/ein-klassiker-ein-comeback-und-eine-weltpremiere-1.17850997>

News.ch, Schweiz\
5. November 2012\
<https://www.news.ch/Die+Buehne+ist+angerichtet+die+Stars+sind+bereit/562612/detail.htm>

Radio NZ, Neuseeland (Englisch)\
1. Februar 2012\
<https://www.radionz.co.nz/national/programmes/afternoons/audio/2508735/big-top>

Otago Daily Times, Otago, Neuseeland (Englisch)\
3. Januar 2012\
<https://www.odt.co.nz/your-town/wanaka/193015/circus-students-put-through-hoops>

Herald Sun, Melbourne, Australien (Englisch)\
26. Mai 2011\
<https://www.adelaidenow.com.au/ipad/acrobats-survive-rough-and-tumble/story-fn6t2xlc-1226063793044>

The Press, Christchurch, Neuseeland (Englisch)\
8. November 2010\
<https://quakestudies.canterbury.ac.nz/store/download/part/230630>

Mitgliedschaften {#mitgliedschaften}
====================================

![equity-member-logo-pantone-2603-low-res.jpg](/img/equity-member-logo-pantone-2603-low-res.jpg){width="228" height="112"}

[![](https://www.procirque.ch/static/badge-am-de.png)](https://www.procirque.ch/de)

---
title: Show-Acts
other-language: acts
colors:
  background: ffaa00
  blocks: ffe600
  links: a36d00
---

Roue Cyr
========

[![PascalHaering-CyrWheel-2-Photo_Simone_Haering-2012.jpg](/img/PascalHaering-CyrWheel-2-Photo_Simone_Haering-2012.jpg){width="575" height="336"}](/de/show-acts/roue-cyr "Roue Cyr")

Roue Cyr ist eine zeitgenössische Zirkusdisziplin, bei der der Mensch
sich in einem Körper-grossen Rad tänzerisch rollt und Kreise
dreht. Diese Nummer verzaubert das Publikum, kreiert wunderschöne Bilder
auf der Bühne und zeigt eine immer noch sehr junge Akrobatikform. Ein
Fest fürs Auge. [Mehr Infos...](/de/show-acts/roue-cyr "Roue Cyr")

Jonglage
========

[![PascalHaering-Juggling-3-Photo_Martin_Potmann-2012.jpg](/img/PascalHaering-Juggling-3-Photo_Martin_Potmann-2012.jpg){width="575" height="338"}](/de/show-acts/jonglage "Jonglage")

Pascals musikalisches Ball-Jonglieren fasziniert durch präzise
Choreographien, welche das Tanzen der Bälle und die Musik zu einem
einzigen Spiel verschmelzen lassen. Ein Vergnügen zum Zuschauen.
[Mehr Infos...](/de/show-acts/jonglage "Jonglage")

Duo Cyr Wheel
=============

[![Duo-Cyr.jpg](/img/Duo-Cyr.jpg){width="575" height="338"}](/de/show-acts/duo-cyr "Duo Cyr")

Beeindruckende, freche und unterhaltende Duo Cyr Wheel Performance mit
Lindyhop Tanzelementen und zu mitreissender Swing-Musik. Dieser Showact
von Esther und Pascal bringt ein ganz spezielles Highlight an Ihren
Event.
[Mehr Info...](/de/show-acts/duo-cyr "Duo Cyr")

Cube/Würfel Manipulation
========================

[![Cube.jpg](/img/Cube.jpg){width="575" height="383"}](/de/acts/cube "Cube")

Ein visuelles Spektakel verspricht diese Nummer, bei der Pascal Häring
einen 1.20 Meter grossen Würfel aus Titanium-Stangen über seinen Kopf
und um sich herum drehen lässt.
[Mehr Info...](/de/show-acts/cube "Cube")

Cyrimba
=======

[![IMG_4864.jpg](/img/IMG_4864.jpg){width="575" height="345"}](/de/show-acts/cyrimba "Cyrimba")

Ein einmaliger Showact, der moderne Marimba-Musik mit Roue-Cyr-Tanz
verbindet. Zwei Weltklasse-Künstler verbinden Musik und Artistik zu
einem Kunstwerk.
[Mehr Info...](/de/show-acts/cyrimba "Cyrimba")

Massgeschneidert für Ihren Anlass
=================================

![DSC_1310-1_2.jpg](/img/DSC_1310-1_2.jpg){width="575" height="339"}

Bieten Sie Ihren Gästen etwas Spezielles und buchen Sie einen von
Pascals Show-Acts. Die Performance kann gerne auch für Ihren Anlass
massgeschneidert werden: Choreographien zu von Ihnen gewünschter Musik,
Gruppennummern, Live-Musik, passende Kostüme und Walk-Acts, ... alles
ist möglich. Gerne organisieren wir Ihnen auch Ihren gesamten Event. Um
Showacts oder Events für Firmen- und Privatanlässe, Varietés und andere
Gelegenheiten zu buchen, kontaktieren Sie mich bitte per Telefon oder
Email (Hier geht's zu den [Kontaktangaben](/de/kontakt/)).

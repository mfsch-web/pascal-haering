---
title: Roue Cyr
other-language: acts/cyr-wheel
colors:
  background: 333333
  blocks: a7d1c9
---

Roue Cyr - Cyr Wheel - Cyr Ring
===============================

Lassen Sie sich mit dem Roue Cyr in die bezaubernde Welt der Artistik
entführen. Die Nummer kombiniert akrobatisches Können, Eleganz und
tänzerische Schönheit zu einem Fest fürs Auge. Der Mensch innen und das
kreiselnde Rad in dem er sich hält bilden eine mitreissende Flut von
Bildern. Mit grenzenloser Dynamik dreht, fliegt und schwebt Pascal über
die Bühne, zieht Kreise und schwingt durch Zeit und Raum, er tanzt mit
dem Rad und das Rad mit ihm.

### Auftritt bei "Die grössten Schweizer Talente 2016"

<iframe src="https://player.vimeo.com/video/159610988?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Cirque du Circle

Buchen Sie für Ihren Event eine atemberaubende, verblüffende,
inspirierende und poetische Performance mit einer zeitgenössischen
Zirkusdisziplin, welche vielen Ihrer Gäste noch unbekannt sein wird.
Pascal's Nummer ist technisch hochstehend, elegant und einfach
bezaubernd.

Länge: 5-6 Minuten

Technische Voraussetzungen:\
- Mindestens 4.5 x 4.5m Bühnenfläche mit hartem Boden\
- Musikanlage

Showact in voller Länge (normale Bühne):\
<iframe src="https://player.vimeo.com/video/281103939?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Showact in voller Länge (grosse Bühne):\
<iframe src="https://player.vimeo.com/video/178342547?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Individualisierung für Ihren Anlass

Wollen Sie eine Nummer choreographiert zu Ihrer Lieblingsmusik? Oder ein
Auftritt mit Live-Musik? Zum Beispiel zu Marimbaphon? Wie wär's mit
einer Feuer- und Cyr Wheel Show? Oder einem kunstvollen Auftritt mit
Projektionen und Schatten? Die Möglichkeiten sind grenzenlos.
Kontaktieren Sie mich!

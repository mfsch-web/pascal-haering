---
title: Duo Cyr
other-language: acts/duo-cyr
colors:
  background: 1c1c1c
  blocks: ad1a30
---

Duo Cyr Wheel
=============

### Swing Wheel

Eine beeindruckende, freche und inspirierende Nummer von zwei weltklasse
Artisten in einem Ring.

Dauer: 5-7 Minuten

Technische Voraussetzungen:\
- Mindest-Bühnengrösse: 5 x 5m\
- Musikanlage

Video:\
<iframe src="https://player.vimeo.com/video/354644912?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>\
SpielerInnen: Pascal Haering und Esther Fuge

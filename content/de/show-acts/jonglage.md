---
title: Jonglage
other-language: acts/juggling
colors:
  background: ff4400
  blocks: ffdd00
---

Jonglage
========

Geniessen Sie perfekt auf die Musik choreographierte Jonglage und
innovative Charakterjonglage mit Ball und einem Spazierstock. Pascal's
Nummern verzaubern durch ihre hohe Musikalität und unterhalten mit
Comedy, Puppenspiel und innovativer Jonglage. Diese Nummern passen zu
jedem Event, und sind die perfekte Auflockerung zwischen anderen
Programmpunkten Ihres Anlasses.

### Schmetterling

Ball-Jonglage mit 3-5 Bällen, exakt zur Musik choreographiert.\
Länge: 5:30 Minuten\
Technische Voraussetzungen:\
- Ungefähr 3m Raumhöhe\
- Musikanlage

Video:\
<iframe src="https://player.vimeo.com/video/225145138?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Ball und Gehstock

Länge: Ungefähr 6-7 Minuten\
Technische Voraussetzungen:\
- Ungefähr 4m Raumhöhe\
- Musikanlage

Video:\
<iframe src="https://player.vimeo.com/video/46420339?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Passing

Länge: Nach Wunsch\
Technische Voraussetzungen:\
- Ungefähr 4m Raumhöhe oder mehr\
- Musikanlage

Video:\
<iframe src="https://player.vimeo.com/video/60703037?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Choreographie zu Ihrer Musik

Geben Sie mir ein Musikstück und ich choreographiere Ihnen einen
Jonglier-Act dazu. Kontaktieren Sie mich.

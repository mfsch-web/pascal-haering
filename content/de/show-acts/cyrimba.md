---
title: Cyrimba
other-language: acts/cyrimba
colors:
  background: 363636
  blocks: ebf7ff
---

Cyrimba
=======

Cyrimba ist eine Kollaboration zwischen der Musikerin und Komponistin
Harriet Riley sowie Zirkusartist und Roue Cyr Spezialist Pascal Häring,
eine Kombination aus eigens komponierter und live gespielter
Marimba-Musik und poetischer Zirkuskunst.

Unser neuer Showact - der für Firmen- und Privatanlässe gebucht werden
kann - ist eine Komposition aus zeitgenössischer Musik und
Choreographie, bei der Musik und Bewegung zu einer wunderschönen Einheit
verschmelzen.

<iframe src="https://player.vimeo.com/video/189149336?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Für weitere Informationen kontaktieren Sie uns bitte unter <info@cyrimba.com>

![IMG_4886.jpg](/img/IMG_4886.jpg){width="575" height="324"}

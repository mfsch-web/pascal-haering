---
title: Cube
other-language: acts/cube
colors:
  background: 143291
  blocks: b0b0b0
---

Cube/Würfel Manipulation
========================

Ein visuelles Spektakel verspricht diese Nummer, bei der Pascal Häring
einen 1.20 Meter grossen Würfel aus Titanium-Stangen über seinen Kopf
und um sich herum drehen lässt. Diese Show ist gleichzeitig imposant und
bezaubernd, eine Zirkusdisziplin, die man nur noch selten sieht. Eine
ideale Ergänzung zur Roue Cyr Nummer.

Länge: 4 Minuten

Technische Voraussetzungen:\
- Erfordert ist eine hindernisfreie Spielfläche mit mindestens 6m
Durchmesser und mindestens 4.5m lichte Raumhöhe über der Bühne. (Die
effektive Bühnenfläche (der Bühnenboden) kann deutlich kleiner sein (2m
x 2m genügen), aber der "freie Luftraum" über der Bühne muss einen
Durchmesser von mindestens 6 Metern haben.)\
- PA system

Video:\
<iframe src="https://player.vimeo.com/video/380377323?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

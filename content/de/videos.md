---
title: Videos
other-language: videos
colors:
  background: a4d6c5
  blocks: 3d3d3d
  text: ffffff
---

Videos
======

(Falls die Videos auf dieser Seite nicht abgespielt werden können, bitte
auf den Video-Titel klicken.)

### [Showreel 2019](https://vimeo.com/281065133)

<iframe src="https://player.vimeo.com/video/281065133?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artist: Pascal Häring

### [Duo Cyr Wheel Trailer](https://vimeo.com/354644912)

<iframe src="https://player.vimeo.com/video/354644912?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artisten: Pascal Häring und Esther Fuge

### [Cube/Würfel Trailer](https://vimeo.com/380377323)

<iframe src="https://player.vimeo.com/video/380377323?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artist: Pascal Häring

### [Roue Cyr @ Firmen-Event](https://vimeo.com/281103939)

<iframe src="https://player.vimeo.com/video/281103939?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Roue Cyr Nummer "Cirque du Cercle", gespielt bei einem Gala-Event im
Februar 2018.\
Artist: Pascal Häring

### [Jonglier-Showact "Butterfly"](https://vimeo.com/225145138)

<iframe src="https://player.vimeo.com/video/225145138?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artist: Pascal Häring

### [Showreel 2016](https://vimeo.com/158477407)

<iframe src="https://player.vimeo.com/video/158477407?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### [Roue Cyr @ Glastonbury Festival 2016](https://vimeo.com/178342547)

<iframe src="https://player.vimeo.com/video/178342547?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Roue Cyr Nummer "Cirque du Cercle" im Circus Big Top des Glastonbury
Festivals 2016.\
Artist: Pascal Häring

### [Auftritt bei "Die grössten Schweizer Talente" 2016](https://www.youtube.com/watch?v=88biuv4lkpU)

<iframe  src="https://www.youtube.com/embed/88biuv4lkpU?showinfo=0" frameborder="0" width="575" height="323"></iframe>

### [Roue Cyr @ M/S Color Fantasy](https://vimeo.com/77131592)

<iframe src="https://player.vimeo.com/video/77131592?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Diese Nummer wurde entwickelt und gespielt für die Show "Pirates of the
Skagerrak" an Bord der Fähre Color Fantasy von Color Line, welche die
Städte Kiel und Oslo miteinander verbindet. Die Aufnahme ist im Oktober
2013 entstanden.\
Artist: Pascal Häring

### [Roue Cyr @ Swiss Christmas 2012](https://vimeo.com/55417250)

<iframe src="https://player.vimeo.com/video/55417250?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Roue Cyr Nummer gespielt an der Swiss Christmas Show 2012: "Diamond
Illusion" in Zürich Oerlikon, 22. November - 31. Dezember 2012, Artist:
Pascal Häring

### [Jongliernummer mit Ball & Stock](https://vimeo.com/46420339)

<iframe src="https://player.vimeo.com/video/46420339?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artist: Pascal Häring, Juli 2012

### [Partner-Akrobatik: "Lederhosen"](https://vimeo.com/24064578)

<iframe src="https://player.vimeo.com/video/24064578?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artisten: Pascal Häring und Jemadeekara Lewandowski-Porter, Cabaret
Vertigo in Melbourne, Mai 2011

Alle Videos: © by Pascal Häring, 2010-2018

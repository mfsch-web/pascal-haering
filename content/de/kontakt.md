---
title: Kontakt
other-language: contact
colors:
  background: 47541d
  blocks: e1ff8f
---

Kontakt
=======

Pascal Häring\
Sissach CH / Bristol GB

Erreichbar unter folgenden beiden Nummern.\
CH: +41 76 38 12345\
GB: +44 753 4400992

Email: <mail@pascalhaering.com>

Facebook: [Pascal Häring / Haering - circus artist](https://www.facebook.com/haeringpascal/)\
Linkedin: [Pascal Häring](https://www.linkedin.com/in/pascal-h%C3%A4ring-3943139)

Newsletter-Anmeldung
====================

Hier könnt Ihr euch für meinen deutschsprachigen Newsletter
einschreiben.

<form action="https://tinyletter.com/pascalhaering" method="post" onsubmit="window.open('https://tinyletter.com/pascalhaering', 'popupwindow', 'scrollbars=yes,width=800,height=600');return true" target="popupwindow">
  <p><label for="tlemail">Bitte E-Mail Adresse angeben:</label></p>
  <p><input id="tlemail" style="width: 240px;" type="text" name="email" /></p>
  <input type="hidden" name="embed" value="1" /><input type="submit" value="Anmelden" />
  <p><a href="https://tinyletter.com" target="_blank">powered by TinyLetter</a></p>
</form>

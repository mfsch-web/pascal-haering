---
title: Home
other-language: welcome
colors:
  background: 001682
  blocks: 00ccff
---

Willkommen…
===========

… in der verrückten Welt des Zirkus.

<span style="color:001682">
BREAKING NEWS: Vom 24. - 26. April spiele ich das Stück _Falling Down the Rabbit Hole_ gemeinsam mit meiner Bühnenpartnerin Esther Fuge im Gundeldinger Feld in Basel. Mehr Info unter [www.cirqueducercle.com](http://www.cirqueducercle.com)
</span>

<iframe src="https://player.vimeo.com/video/805703677?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Pascal Häring bietet begeisterndes und hochstehendes Entertainment für
jeden Anlass. Seine Show-Acts können für Ihren Event massgeschneidert
werden. Mit über 600 Shows in den letzten 12 Jahren verfügt er über eine
riesige Bühnenerfahrung und wird für ein unvergessliches Erlebnis Ihrer
Gäste sorgen.

<iframe src="https://player.vimeo.com/video/281065133?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Vor mehr als 5000 Jahren erfanden die Sumerer das Rad. Seither ist es
ein unverzichtbarer Bestandteil unseres täglichen Lebens geworden. Dass
es aber auch zur Grundlage künstlerischen Ausdrucks werden würde,
konnten die Sumerer noch nicht erahnen. Lassen Sie sich mit einer erst
15 Jahre alten Version des Rades, dem Roue Cyr, in die bezaubernde Welt
der Artistik entführen.

***"er erschafft beeindruckende Bühnenbilder"***\
(Stadtspiegel Bochum)

***"es war wirklich ein sehr schöner und stimmungsvoller Auftritt, der uns allen lange in Erinnerung bleiben wird"***\
(Firmenkunde)

***"tänzerischer Eleganz zu wunderschöner Musik"***\
(THERAPIE - Das Lifestylemagazin)

***"die Zuschauer waren sehr berührt von Deinem Auftritt - die Show war ein voller Erfolg - die Feedbacks überwältigend"***\
(Direktorin von Art on Stage)

***"Ihre Nummer war mehr als super! – Vor allem in Verbindung mit der Musik fand ich sie sozusagen 'traumhaft' "***\
(Zuschauerin)

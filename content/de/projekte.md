---
title: Projekte
other-language: artistic
colors:
  background: 171717
  blocks: adadad
  links: 870303
---

Kreative Projekte
=================

Im Folgenden sind einige Beispiele aktueller und vergangener künstlerischer und
kreativer Projekte aufgeführt, welche ich in Kooperation mit anderen
Artist\*innen entwickeln und durchführen durfte.

### Cirque du Cercle

Cirque du Cercle ist meine Schweizer Company, organisiert als Verein mit Sitz in Sissach. Im 2022 haben wir mit Cirque du Cercle unsere erstes zeitgenössisches Zirkusstück kreiert: _Falling Down the Rabbit Hole_, von und mit Esther Fuge und mir, unter der Regie von Delia Dahinden.

**_Falling Down the Rabbit Hole_ spielen wir vom 24. bis 26. April im Gundeldinger Feld in Basel. Mehr Informationen dazu gibt's auf der Webseite von Cirque du Cercle: [www.cirqueducercle.com](http://www.cirqueducercle.com)**

[![2208_CirqueduCercle_230.jpg](/img/2208_CirqueduCercle_230.jpg){width="560" height="373"}](/de/projekte/cirque-du-cercle "Cirque du Cercle")
Bild der Premiere von _Falling down the Rabbit Hole_. Artist*innen: Esther Fuge and Pascal Haering. Bild: Mina Monsef

### In Colour

Als Koproduzent und Darsteller von *Swing Circus* habe ich gemeinsam mit
Laura James, Lana Williams, Moses Opiyo, Francis Odongo und Ezra Weill
das Stück *In Colour* kreiert, welches im Oktober 2018 im *Circomedia
Centre for Contemporary Circus and Physical Theatre* Premiere feierte
und anschliessend am *Salisbury International Festival* 2019 gezeigt
wurde. *In Colour* verbindet Zirkus und Swing-Tanz (Lindy Hop), ist
inspiriert von der Geschichte des Swings und thematisiert das Thema
Rassismus damals und heute. [Hier erfahren Sie mehr über den Swing Circus](https://www.swingcircus.co.uk/ "Swing Circus").

<iframe  style="font-size: 10px;" src="https://www.youtube.com/embed/XlBorfYxWIE" frameborder="0" width="560" height="315"></iframe>

### Coppélia

Unter der Regie von Joanna Vymeris war ich im Frühling 2017 beteiligt an
der Kreation von *Coppélia*, einer zeitgenössischen Zirkus-Adaption des
gleichnamigen klassischen Ballettstücks, produziert von *Feathers of
Daedalus Circus*. [Mehr Informationen zu Coppélie und The Feathers of Daedalus Circus gibt es hier](https://www.featherscircus.uk/about "The Feathers of Daedalus Circus").

<iframe  src="https://www.youtube.com/embed/LF2kdrToXS4" frameborder="0" width="560" height="315"></iframe>

### Cyrimba

Ein Stück, welches zeitgenössische Marimba-Musik und Cyr Wheel Tanz
verbindet und verwebt. Eine Kreation in Zusammenarbeit mit der Musikerin
Harriet Riley, entstanden 2016/2017. [Mehr Informationen zu Cyrimba sind hier zu finden](https://pascalhaering9.wixsite.com/cyrimba "Cyrimba").

<iframe src="https://player.vimeo.com/video/189149336?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Esperanza - Arbeiten mit Projektionen und Schatten

Ein artistisches Projekt in Zusammenarbeit mit der Graphikdesignerin
Conny Schwark, entstanden im Mai 2014.

<iframe src="https://player.vimeo.com/video/96773184?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

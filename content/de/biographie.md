---
title: Biographie
other-language: about
colors:
  background: ff0022
  blocks: ffa296
---

Vom Physiker zum Zirkusartisten
===============================

![pascalhaering, head shot, Photography by Simone
Haering](/img/dbcc5f67dbe704ab231b9c2ad60eca70_f64.jpg){.float width="320"
height="214"}
Aufgewachsen im schweizerischen Sissach zeigte Pascal
Häring schon früh Bewegungstalent, fuhr Ski, spielte Unihockey,
jonglierte und tanzte. Trotzdem entschied er sich vorerst für eine
wissenschaftliche Laufbahn und arbeitete nach erfolgreichem **Abschluss
des Physikstudiums** bei der Sensirion AG in Stäfa, einem jungen, schnell
wachsenden Unternehmen der **Elektronikindustrie**.

Einige Jahre später machte seine Karriere einen Quantensprung als er
sich dazu entschied, seinem Traum zu folgen. Im Januar 2010 tauschte er
seinen Arbeitsplatz gegen eine Trainingshalle, zog von der Schweiz nach
Neuseeland und startete eine zweijährige Ausbildung zum Artisten an der
**Zirkusschule CircoArts in Christchurch**. Erdbebenbedingt wechselte er ein
Jahr später ans „National Institute of Circus Arts" in Melbourne, wo er
seine Fähigkeiten weiter ausbauen und verfeinern konnte. Seither war er
unter anderem mit dem Circus Aotearoa auf Neuseeland-Tournee, spielte in
den Weihnachts-Shows "Swiss Christmas" in Zürich und „Noël en Cirque"
in Valence d'Agen, im Varieté Et Cetera in Bochum, sowie auf
Kreuzfahrtschiffen, an Varieté-Abenden, Festivals, Firmen- und
Privatanlässen. 2014/2015 war Pascal Häring für 12 Monate mit dem
**Musical Barnum auf Grossbritannien-Tournee** und im Februar 2016 konnte
man seine Fähigkeiten in der **TV-Show "Die grössten Schweizer Talente"**
bewundern. Gemeinsam mit seiner Duo-Partnerin Esther Fuge spielt er seit
2019 regelmässig in der Show CARO des Efteling Freizeitparks in den
Niederlanden.

Pascal ist spezialisiert auf **Roue Cyr, Jonglage und Würfel-Manipulation**. Er
beeindruckt durch technisches Können, eine faszinierende Bühnenpräsenz
und eine aussergewöhnliche Musikalität. Dank genauer Abstimmung zwischen
Musik, Artistik und Performance verschmelzen die verschiedenen Elemente
zu einem fesselnden Gesamtkunstwerk.

Daneben hat Pascal auch langjährige Erfahrung im Unterrichten von Sport
und Artistik. Er besitzt das Schweizerische Schneesportlehrer-Diplom und
arbeitet seit über 20 Jahren nebenbei als Ski-, Unihockey- und
Zirkuslehrer.

Im Dezember 2020 schloss Pascal den **Weiterbildungsstudiengang "Executive Master in Arts Administration"** an der Universität Zürich erfolgreich ab. Diese Ausbildung bildet eine wichtige Grundlage sowohl für das Produzieren eigener Shows und Projekte, als auch für den zukünftigen Einstieg in das Management einer Kulturorganisation. [Hier](/Ticketpreise_im_Theater.pdf "Ticketpreise im Theater") geht es zur Abschlussarbeit über Ticketpreissysteme in Theatern der Freien Szene in der Schweiz.

[Download: Lebenslauf](/CV_PascalHaering_Deutsch.pdf)

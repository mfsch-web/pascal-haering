---
title: pascalhäring – roue cyr
home: true
colors:
  background: 8f0808
  blocks: ff6600
description: Pascal Häring ist ein Schweizer Zirkusartist spezialisiert auf Roue Cyr (Cyr Wheel), Jonglage und Partnerakrobatik. Er bietet Weltklasse-Entertainment für Shows, Events, Hochzeiten und Festivals.
keywords: Pascal, Häring, cyr wheel, roue cyr, Rad, Ring, Zirkusartist, Artist, Performer, Jongleur, jonglieren, Akrobatik, Partnerakrobatik, Hand auf Hand, Show Acts, Events, Galas, Hochzeiten, Shows, Entertainment, Unterhaltung, Sissach, Basel, Schweiz, Videos, Filme, DGST, Die grössen Schweizer Talente
nav-order:
  - welcome
  - willkommen
  - about
  - biographie
  - news
  - acts
  - acts/cyr-wheel
  - acts/juggling
  - acts/duo-cyr
  - acts/cube
  - acts/cyrimba
  - show-acts
  - show-acts/roue-cyr
  - show-acts/jonglage
  - show-acts/duo-cyr
  - show-acts/cube
  - show-acts/cyrimba
  - artistic
  - projekte
  - videos
  - pictures
  - fotos
  - references
  - referenzen
  - teaching
  - teaching/january-2017
  - teaching/march-2018
  - teaching/may-2018
  - teaching/july-2018
  - teaching/augsep-2018
  - teaching/nov-2019
  - teaching/mar-2020
  - unterricht
  - kontakt
---

<img src="/img/welcome.jpg" alt="welcome.jpg" width="580" height="447" usemap="#welcome-map" />

<map name="welcome-map">
  <area shape="rect" coords="130,20,380,70" href="/de/willkommen" />
  <area shape="rect" coords="10,190,220,240" href="/en/welcome" />
  <area shape="rect" coords="470,370,570,440" href="https://www.facebook.com/haeringpascal/" target="_blank" />
</map>

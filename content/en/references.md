---
title: References
other-language: referenzen
colors:
  background: 155452
  blocks: ccb6b6
---

Testimonials
============

[References](#references) – [Awards](#awards) – [In the media](#press) - [Memberships](#memberships)

**Sensirion AG**\
*Hi Pascal\
Many thanks to you – it really was a very beautiful and atmospheric
performance, which we will all remember for a long time.\
Kind regards,\
Pamela*

**Art on Stage, Biel**\
*Hello Pascal\
Heartfelt thanks for your brilliant interpretation of the song. I'm
very happy to have met you.\
Your artistic interpretation was exactly to my taste and confirms my
inquiry to you. Also the audiences were very touched by your
performance.\
The show was a complete success. The feedbacks overwhelming.\
Dani*

**Monika Gerhardt, member of the audience at Varieté et cetera**\
*Hi Pascal\
I was at the special event of Varieté et cetera today and your act was
more than excellent.\
Specifically in connection to the music it felt "dreamlike" to me.\
Kind regards and a lot of success\
Monika Gerhardt*

**Andreas Wølner-Hanssen, lecturer physical education at University of
Basel**\
*Hello Pascal\
I'm watching Switzerland's got Talent and am amazed by your
performance. It is so incredibly hard what you show with absolute ease.
I am a lecturer at the department of sport, movement and health at the
University of Basel and teach classes in "art of movement", which is
exactly your field.\
With sporty regards,\
Andreas*

All testimonials translated from German

References {#references}
========================

### 2020

Royal Shakespeare Company, Stratford-upon-Avon UK\
CARO, Efteling Park / Imagine Shows, Tilburg NL

### 2019

CARO, Efteling Park / Imagine Shows, Tilburg NL\
Eisenbahner-Baugenossenschaft beider Basel, Basel CH\
IN COLOUR, Swing Circus, Salisbury International Festival, Salisbury UK\
LED Cyr Wheel, Remy Martin / Smoking Hot Productions, Lagos Nigeria\
Kantonsschule Wil, Maturaball, Wil SG, CH\
Bucher Municipal, Sommerfest, Niederweningen CH\
Shopville, Zurich CH\
European Juggling Convention, Gala Show, Newark UK\
Glenmark, Zurich CH\
Smoking Hot Productions, Brighton UK\
Motorsport Switzerland, Bern CH\
SBB Immobilien, Pfaffikon CH\
AECC University College, Bournemouth UK\
Kunststoff Schwanden, Schwanden CH

### 2018

Flying Colours, Edwardian Gala, Lotherton Hall, Leeds UK\
Organised Kaos, Nandos Event, Newport UK\
The Mayor of Barnett Gala Ball, UK\
Al Kout Mall, Kuwait\
Bristol Swing Festival 2018, Bristol UK\
Evonik, Rheinfelden GER\
Sportamt Zug, Zuger Sportnacht 2018, Zug CH\
Tattoo & Art Show, Loerrach GER\
Photon Cyr Wheel, Smoking Hot Productions, London UK\
Swing Circus, Beautiful Days Festival, Devon UK\
Swing Circus, R&D and Premiere von IN COLOUR, Circomedia Bristol UK\
Salto Sociale, Benefiz Gala, Loerrach GER

### 2017

Christmas Circus Lörrach, GER\
LR Health & Beauty Systems, Zurich CH\
Tumbellina, Mall of Asia, Manila PHL\
Circus City Festival, Swing Circus, Bristol UK\
ÔlaC Festival 2017, Milvignes / Neuchâtel CH\
Insight Ensemble goes Outer-Space!, Bristol UK\
Tilburg Fietst Festival (Cycling Festival), Tilburg NL\
Schule Bethlehemacker, Bern CH\
Valley Fest, Chew Valley Lake UK\
Bristol Harbour Festival, Circue Bijou, Bristol UK\
Circus Fantastic, Bristol UK\
Pfizer, Surrey UK\
Coppélia, a 'The Feathers of Daedalus' production, Jackson's Lane,
London UK\
Bristol Swing Dance Festival, Bristol UK\
Impro Cirque, Circus Improvisation Match, Monthey CH\
Arosa Snow Show, Arosa CH\
Basel School Dance Award, Lausen CH

### 2016

Best Parties Ever, Malvern UK\
UNIA, Schaffhausen CH\
Performers Without Borders, Fundraiser, Bristol UK\
BACA Workwear and Safety, UK\
Heathrow Charity Ball, Heathrow UK\
Tumbellina International, UK\
Ziefener Schauturnen, Ziefen CH\
British Acrobatics Festival, Oxford UK\
Jugendzirkus Prattelino, Pratteln CH\
Clinam (European Foundation for Clinical Nanomedicine), Basel CH\
Glastonbury Festival, Circus Big Top, Glastonbury UK\
Invisible Circus, Bristol UK\
Zirkuswerkstatt Liestal, Teaching circus, Liestal CH\
Kinder-Camps, Teaching Acrobatics and Juggling, Sissach CH\
Albany Circus Cabaret, Planet Bowie, Bristol UK\
Switzerland's got Talent, TV show, SRF 1, Zurich CH

### 2015

Wie-n en Stärn, wo am Himmel stoht; Aarau CH\
Art on Stage, Biel-Bienne CH\
Camvention Gala Show, Cambridge UK\
Barnum Musical, Cameron Mackintosh, UK Tour\
Sensirion AG, Corporate event, Zurich CH

### 2014

Noël en Cirque, Valence d'Agen F\
Barnum Musical, Cameron Mackintosh, UK Tour\
- Curve Theatre, Leicester\
- Bristol Hippodrome\
- Cliffs Pavilion, Southend\
- Wycombe Swan\
- New Wimbledon Theatre\
- Hull New Theatre\
- Edinburgh Playhouse\
- Manchester Palace\
- His Majesty's Theatre, Aberdeen\
- Bradford Alhambra\
Boulder Juggling Festival, Gala, Boulder US-CO\
Dräger, Corporate event, 125th anniversary of Draeger, Lübeck GER\
Greentop Cabaret, Sheffield UK\
Circus North Incubator, Greentop Circus, Sheffield UK

### 2013

Festival des Artistes de Cirque, Saint-Paul-lès-Dax F\
Birthday party, 70th anniversary of Ueli Pfister, Liestal CH\
M/S Color Fantasy, Color Line, Ferry between Kiel (GER) and Oslo (N)\
Cirque Bouffon, Nandou, Cologne GER\
Varieté Et Cetera, "Weggezapped", Bochum GER\
Tasmanian Circus Festival, Golconda (TAS) AUS

### 2012

Swiss Christmas, Diamond Illusion, Zurich Oerlikon CH\
Schule für Körper- und Mehrfachbehinderte, Zurich CH\
FAQoustic Swiss Talent Tour, Rheinfelden (Baden) GER\
Circus Gasser Olympia, Gala Show for Ernst Frey AG, Kaiseraugst CH\
Swiss Juggling Convention, Wetzikon CH\
European Juggling Convention, Lublin PL\
Monday Night Magic @ The Dark Room, Christchurch NZ\
TV2 KidsFest, The Flying Kiwi Circus Show, Christchurch NZ\
Christchurch Circus Centre Open Night, Christchurch NZ\
Cabaret Saveloy, The Ice Cream Factory, Brisbane AUS\
The Woohoo Revue concert, Wollongong AUS\
Private function, Wollongong AUS\
Circus Aotearoa, New Zealand (3 months season)

### 2011

Melbourne Fringe Festival, "This old city", AUS\
Melbourne Juggling Convention, AUS\
Melbourne Circus Festival, AUS\
National Institute of Circus Arts, "Lost Ground", Melbourne AUS\
Variety Collective, Melbourne AUS\
Club Voltaire, "Month of Sundays", Melbourne AUS\
Cabaret Vertigo, Melbourne AUS\
Earthquake relief and welfare centres in Christchurch NZ

### 2003 – 2010

Circus under the stars, Christchurch NZ\
CPIT school of performing arts, "Finale", Christchurch NZ\
Zirkus Stern, Sempach CH (3 seasons)\
Trade show, Gelterkinden CH\
University ball, Basel CH\
Arosa Tourism Snowsport Show, Arosa CH\
Circus Prattelino, Pratteln CH\
Various private functions, CH

Awards {#awards}
================

Boulder Circus Award 2014

In the media {#press}
=====================

Below a few news articles, radio interviews and online videos covering
Pascal's career.

Employee magazine of ING-DiBa, "Ich sag's euch" (in German)\
März 2017\
<https://du.ing-diba.de/archiv/2017_01/ich_sag_s_euch.html>

Aroser Zeitung, Switzerland (in German)\
24 February 2017\
[Article Aroser Zeitung](/Article-AroserZeitung-Feb2017.pdf)

Regio aktuell, Switzerland (in German)\
May 2016\
<https://www.regio-info.ch/images/stories/regio-artikel/2016/05-16/pdf/ausgewandert.pdf>

Schweizer Illustrierte Online (in German)\
27 February 2016\
<https://www.schweizer-illustrierte.ch/stars/schweiz/dgst-2016-pascal-haering-vor-jury-jonny-fischer>

Beruf + Berufung (Bund, Berner Zeitung, Tamedia-Newsnet, Ostschweiz am
Sonntag, ostjob.ch, beruf-berufung.ch), Switzerland (in German)\
28 November 2015\
<https://blog.derbund.ch/berufung/index.php/34920/wenn-man-wenig-plant-bleibt-mehr-zeit-fuer-schoene-ueberraschungen/>

Migros Magazin, Switzerland (in German)\
2 November 2015\
<https://www.migrosmagazin.ch/menschen/portraet/artikel/zirkusartist-haering-und-sein-roue-cyr>

Radio SRF 1: "Die fünfte Schweiz", Switzerland (in German)\
25 October 2015\
<https://www.srf.ch/sendungen/die-fuenfte-schweiz/pascal-haering-mit-dem-zirkusrad-um-die-welt>

Bay TV, Liverpool, United Kingdom\
20 May 2015\
<https://youtu.be/WSqJ8i-0aWM?t=7m6s>

Sonntagsnachrichten, Herne, Germany (in German)\
February 2013\
<https://www.sn-herne.de/index.php?cmd=article&aid=5314>

THERAPIE - Das Lifestylemagazin, Essen, Gerany (in German)\
February 2013\
<https://www.therapie-online.de/home/artikel/view/58/weggezapped/>

NZZ, Zurich, Switzerland (in German)\
27 November 2012\
<https://www.nzz.ch/zuerich/ein-klassiker-ein-comeback-und-eine-weltpremiere-1.17850997>

News.ch, Switzerland (in German)\
5 November 2012\
<https://www.news.ch/Die+Buehne+ist+angerichtet+die+Stars+sind+bereit/562612/detail.htm>

Radio NZ, New Zealand\
1 February 2012\
<https://www.radionz.co.nz/national/programmes/afternoons/audio/2508735/big-top>

Otago Daily Times, Otago, New Zealand\
3 January 2012\
<https://www.odt.co.nz/your-town/wanaka/193015/circus-students-put-through-hoops>

Herald Sun, Melbourne, Australia\
26 May 2011\
<https://www.adelaidenow.com.au/ipad/acrobats-survive-rough-and-tumble/story-fn6t2xlc-1226063793044>

The Press, Christchurch, New Zealand\
8 November 2010\
<https://quakestudies.canterbury.ac.nz/store/download/part/230630>

Memberships {#memberships}
==========================

![equity-member-logo-pantone-2603-low-res.jpg](/img/equity-member-logo-pantone-2603-low-res.jpg){width="228" height="112"}

[![](https://www.procirque.ch/static/badge-ma-fr.png)](https://www.procirque.ch)

[![Pascal Haering - Bristol for hire at Entertainers Worldwide](https://www.entertainersworldwide.com/profile-badge/80562/ew-registered-s.png)](https://www.entertainersworldwide.com/speciality-variety-acts/pascal-haering-80562 "View Pascal Haering -  Profile")

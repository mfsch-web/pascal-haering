---
title: Artistic
other-language: projekte
colors:
  background: 171717
  blocks: adadad
---

Artistic Work
=============

This sections shows examples of past research and artistic projects.

### Cirque du Cercle

Cirque du Cercle is my Swiss company and a "Verein" (community interest company) based in Sissach CH, Switzerland. In 2022, we deceloped our first show with Cirque du Cercle: _Falling down the Rabbit Hole_. **The next performances of _Falling Down the Rabbit Hole_ will be from 24th to 26th April 2023 in Basel. For more information: [www.cirqueducercle.com](http://www.cirqueducercle.com).**

[![2208_CirqueduCercle_230.jpg](/img/2208_CirqueduCercle_230.jpg){width="560" height="373"}](/en/artistic/cirque-du-cercle "Cirque du Cercle")
From the premiere of _Falling down the Rabbit Hole_. Artists: Esther Fuge and Pascal Haering. Photorgraphy: Mina Monsef

### In Colour

As co-producer and performer of *Swing Circus* I created this show
together with Laura James, Lana Williams, Moses Opiyo, Francis Odongo
und Ezra Weill. *In Colour* was created in October 2019, premiered
at *Circomedia Centre for Contemporary Circus and Physical Theatre* and
has later been performed at the *Salisbury International Festival* 2019.
The show connects circus and swing dance, was inspired by the history of
swing and shines a light racism in the swing era and today.

<iframe  src="https://www.youtube.com/embed/XlBorfYxWIE" frameborder="0" width="560" height="315"></iframe>

### Coppélia

Produced by *Feathers of Daedalus Circus* and directed by Joanna
Vymeris, *Coppélia* is a modern contemporary circus adaption of the
classical ballet with the same name. I was involved in the R&D and
performed the piece at Jacksons Lane in 2017

<iframe  src="https://www.youtube.com/embed/LF2kdrToXS4" frameborder="0" width="560" height="315"></iframe>

### Cyrimba

A unique project that combines original contemporary marimba music and
cyr wheel dance, created in cooperation with musician Harriet Riley.

<iframe src="https://player.vimeo.com/video/189149336?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Esperanza - Working with projections and shadows

An artistic project with design artist and video animator Conny Schwark.

<iframe src="https://player.vimeo.com/video/96773184?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

---
title: Cirque du Cercle
other-language: projekte/cirque-du-cercle
colors:
  background: 7d0000
  blocks: ffffff
  text: 000000
---

Cirque du Cercle
================

Cirque du Cercle is a Swiss "Verein" (community interest company) with
the aim to facilitate and support circus performers connected to the
wider area of Basel and to produce contemporary circus projects and
shows.

Find more information about Cirque du Cercle and the new show "Falling Down the Rabbit Hole" under [www.cirqueducercle.com](http://www.cirqueducercle.com). The next performances of "Falling Down the Rabbit Hole" will be from 24th to 26th April 2023 in Basel.

<!--
### Falling Down the Rabbit Hole


#### Show development

Two artists unexpectedly find themselves in a surreal world full of absurdities, illusions, and par-adoxes. Like Alice in Wonderland, they are fascinated by the strange occurrences around them and lose themselves deeper and deeper in the seemingly unreal world. Falling Down the Rabbit Hole is a playful, poetic, and absurd piece of contemporary circus and a metaphor for how easy it is to get lost in the infinite, sometimes surreal depths of the internet and other sources of infor-mation, and how blurred the boundaries between reality and fiction have become.

It has never been easier to get information, and at the same time it has never been more difficult to find one's way in the seemingly infinite universe of knowledge, data, entertainment, and distractions. Today, around 1.2 billion websites can be found on the internet, and distinguishing between the cor-rect, the important, the useless and the wrong is sometimes a challenge that is almost impossible to master.

In Lewis Carroll’s tale from 1865, Alice falls down the rabbit hole as she follows a clothed white rabbit with a pocket watch and lands in a world full of wonders. Today, the expression “falling down the rabbit hole” has become synonymous with getting lost in a thicket of distractions and (mis)information. Or, as Kathryn Schulz writes in The New Yorker: “When we say that we fell down the rabbit hole, […] we mean that we got interested in something to the point of distraction – usually by accident, and usually to a degree that the subject in question might not seem to merit.”
She continues, "As a metaphor for our online behavior, however, the rabbit hole has an advantage those other fictional portals lack: it conveys a sense of time spent in transit. In the original story, Alice falls for quite a while—long enough to scout out the environment, grab some food off a passing shelf, speculate erroneously about other parts of the world, drift into a reverie about cats, and nearly fall asleep. Sounds like us on the Internet, all right. In the current use of ‘rabbit hole,’ we are no longer necessarily bound for a wonderland. We’re just in a long attentional free fall, with no clear destination and all manner of strange things flashing past."

In Falling Down the Rabbit Hole, the two artists fall down their very own rabbit hole into an absurd world of information illusions, absurdities, and paradoxes. Rabbit holes can be deep and hard to es-cape. They can lead us down a path of knowledge and discovery, but they can also lead us to lies, misinformation and deception. They are a paradise for science lovers and conspiracy theorists alike, and it can often be difficult to know where to find the truth. Who is lying to us and who isn’t? Which rabbit hole shows reality, and which one deception? Can we trust what we don’t know to be true? This show is inspired by the artists’ reflection on topics such as information overload, conspiracy the-ories, the tipping point between reason and madness and how to set a moral compass in all this mess. 

Together with director Delia Dahinden, Swiss artist Pascal Häring and Welsh artist Esther Fuge are developing an approx. 60-minute contemporary circus show. Through improvisation and devising, they are building a fantastic world in the shallows of the rabbit hole. It will be a world of circular shapes and fabrics, of illusions and absurdities, a world where everything these artists have known to be true tricks them. Flashes of Alice in Wonderland will remind us that things aren’t always what they seem, and people aren’t who they say they are. We are excited to see how the journey through this world will develop over the next few months. When it premieres in August, we will know more.


Many thanks to the following organisations, who are supporting the creation of _Facts and Fake News_ financially:

![FA_BSBL_TUT_rgb_screen-2.png](/img/FA_BSBL_TUT_rgb_screen-2.png){width="400"}

![WAI_ACW_WG_LOT.jpg](/img/WAI_ACW_WG_LOT.png){width="580"}

![MOB_d_RGB_rot.svg](/img/MOB_d_RGB_rot.svg){width="250"}&nbsp;&nbsp;![EGS_Schriftzug_2_schwarz.jpg](/img/EGS_Schriftzug_2_schwarz.jpg){width="200"}

&nbsp;&nbsp;![Logo_Kultur_Bildung_RGB.jpg](/img/Logo_Kultur_Bildung_RGB.jpg){width="200"}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![10653-logo-zirkusquartier-zurich.png](/img/10653-logo-zirkusquartier-zurich.png){width="150"}

![Wallach.png](/img/Wallach.png){width="200"}&nbsp;&nbsp;&nbsp;&nbsp;![Logo_FGE_MK_rgb_72dpi_DE.png](/img/Logo_FGE_MK_rgb_72dpi_DE.png){width="250"}




#### Research
In October 2020, 2021 and September 2021, we carried out three artistic residencies with the Welsh artist Esther Fuge and the Sissach artist Pascal Häring to develop and test themes, ideas and artistic "language" for increased collaboration. Under the working title _(Wo)men_, we initially focussed on the topic of gender roles, gender status gaps and power dynamics. How can we address unhealthy gender dynamics and expose them on stage? The process led us to a reflection on "lies" and finally to the topic "Facts and Fake News" described below, which immediately fascinated us because of its topicality.

For some of the reserach, we worked together with Welsh director Gwen Scott and the Swiss directors Delia Dahinden and Philipp Boë.

We would like to thank the following funders who are a crucial support
to this project:

![farbig_d.gif](/img/5e435a92abf9f7e27413fe656de0a94b_f154.gif){width="200" height="75"}

![Logo_Kultur_Bildung_RGB.jpg](/img/Logo_Kultur_Bildung_RGB.jpg){width="150" height="64"}

**Einwohnergemeinde Sissach**

**Bürgergemeinde Sissach**

-->
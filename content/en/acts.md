---
title: Acts
other-language: show-acts
colors:
  background: ffaa00
  blocks: ffe600
  links: a36d00
---

Cyr Wheel
=========

[![PascalHaering-CyrWheel-2-Photo_Simone_Haering-2012.jpg](/img/PascalHaering-CyrWheel-2-Photo_Simone_Haering-2012.jpg){width="575" height="336"}](/en/acts/cyr-wheel "Cyr Wheel")

Cyr Wheel is a contemporary circus discipline, where the performer spins
and rolls in a simple, body-sized metal wheel. This act mesmerises the
audience, creates beautiful images on stage and shows a type of
acrobatics a lot of people haven't witnessed yet.
[More information...](/en/acts/cyr-wheel "Cyr Wheel")

Juggling
========

[![PascalHaering-Juggling-3-Photo_Martin_Potmann-2012.jpg](/img/PascalHaering-Juggling-3-Photo_Martin_Potmann-2012.jpg){width="575" height="338"}](/en/acts/juggling "Juggling")

Pascal's musical ball juggling act fascinates through the precise
choreography making the music and the juggling appear as one whole.
Subtle character work make them a joy to watch. He can also offer a
juggling roving act or a duo club passing act.
[More information...](/en/acts/juggling "Juggling")

Duo Cyr Wheel
=============

[![Duo-Cyr.jpg](/img/Duo-Cyr.jpg){width="575" height="383"}](/en/acts/duo-cyr "Duo Cyr")

An impressive, entertaining and cheeky duo cyr wheel act, inspired by
swing dance or adapted to your event theme.
[More information...](/en/acts/duo-cyr "Duo Cyr")

Cube Manipulation
=================

[![Cube.jpg](/img/Cube.jpg){width="575" height="383"}](/en/acts/cube "Cube")

A highly visual act, where Pascal Haering spins a 1.20-meter titanium
cube above his head and around his body.
[More information...](/en/acts/cube "Cube")

Cyrimba
=======

[![IMG_4864.jpg](/img/IMG_4864.jpg){width="575" height="345"}](/en/acts/cyrimba "Cyrimba")

A unique performance that combines original contemporary marimba music
and cyr wheel dance. A brilliant fusion of music and acrobatics.
[More information...](/en/acts/cyrimba "Cyrimba")

Bespoke entertainment
=====================

![DSC_1310-1_2.jpg](/img/DSC_1310-1_2.jpg){width="575" height="339"}

Acts and performances can be tailored to your event. Do you have a
favourite music to choreograph an act to? Are you interested in group
performances and choreographies? Would you like to have live music with
the performance? A special costume idea? Or do you want us to organise
your whole event? Contact us for bookings or in case of any questions.
[More information...](/en/contact "Contact")

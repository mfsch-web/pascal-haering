---
title: News
other-language: news
colors:
  background: 2c5299
  blocks: ebebeb
---

Calendar
========

[2023]{style="font-size: 1.17em;"}

24 April 2023, 20:00 - _Falling Down the Rabbit Hole_ at Gundeldinger Feld, Basel. See [www.cirqueducercle.com](http://www.cirqueducercle.com)

25 April 2023, 20:00 - _Falling Down the Rabbit Hole_ at Gundeldinger Feld, Basel. See [www.cirqueducercle.com](http://www.cirqueducercle.com)

26 April 2023, 14:00 - _Falling Down the Rabbit Hole_ at Gundeldinger Feld, Basel. See [www.cirqueducercle.com](http://www.cirqueducercle.com)

23 June 2023 - Gala in Bülach (CH)

2023 (all year) - UK tour with _Cirque - The Greatest Show_, See [www.entertainers.co.uk/show/cirque-the-greatest-show](https://www.entertainers.co.uk/show/cirque-the-greatest-show)


News on Facebook
================

\
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-page" data-href="https://www.facebook.com/haeringpascal" data-tabs="timeline" data-width="500" data-height="5000" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/haeringpascal"><a href="https://www.facebook.com/haeringpascal" target="_blank">Pascal Häring / Haering - circus artist</a></blockquote></div></div>

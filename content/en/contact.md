---
title: Contact
other-language: kontakt
colors:
  background: 47541d
  blocks: e1ff8f
---

Contact
=======

Pascal Häring\
Bristol UK / Sissach CH

You can contact me on both of these phone numbers:\
UK: +44 753 4400992\
CH: +41 76 38 12345

Email: <mail@pascalhaering.com>

Facebook: [Pascal Häring / Haering - circus artist](https://www.facebook.com/haeringpascal/)\
Linkedin: [Pascal Häring](https://www.linkedin.com/in/pascal-h%C3%A4ring-3943139)

---
title: About
other-language: biographie
colors:
  background: ff0022
  blocks: ffa296
---

Physicist-turned-circus-artist
==============================

![pascalhaering, head shot, Photography by Simone Haering](/img/dbcc5f67dbe704ab231b9c2ad60eca70_f64.jpg){.float width="320" height="214"}
Born in Switzerland, Pascal has always been active in sports, dance and
circus, but he pursued a career in science first. After graduating with
a **Masters in Physics** and working in the **electronics industry**, his
life made a quantum leap. He decided to run away to the circus, turn his
hobby into a profession and enrol at the **full-time circus school
CircoArts in New Zealand**. A year later, following a devastating
earthquake, he continued his training at the National Institute of
Circus Arts in Melbourne, where he graduated in December 2011,
specialising in cyr wheel, juggling and partner acrobatics.

He has since gained a wealth of international performance experience,
enchanting the stages of Circus Aotearoa in New Zealand, "Swiss
Christmas" in Zurich, Varieté Et Cetera in Bochum and on board M/S Color
Fantasy, to name but a few. His cyr wheel act was a success at the
Melbourne Fringe Festival and the Circus Festival of Saint-Paul-lès-Dax.
In 2014/2015 he performed in the ensemble of the **Barnum Musical UK tour**.
Since 2019 he works part-time in the show CARO at Efteling theme park in
the Netherlands, performing duo cyr wheel together with Esther Fuge.

The **cyr wheel**, a body-sized ring named after a Canadian circus artist,
is Pascal's favourite circus apparatus. For him, performing with his
wheel is like dancing with a partner. With daring skill and elegance he
inspires and mesmerises his audiences.

Pascal also has a long-standing experience as a sports and circus
teacher. He holds the Swiss Ski Instructor Diploma and has been casually
working as a ski, sports and circus instructor for over 20 years.

In 2020, he completed the **"Executive Master in Arts Administration"** at the Univercity of Zurich.

[Download CV](/CV_PascalHaering_English.pdf)

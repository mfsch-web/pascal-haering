---
title: Home
other-language: willkommen
colors:
  background: 001682
  blocks: 00ccff
---

Welcome…
========

… to the upside down world of circus.

<span style="color:001682">
BREAKING NEWS: From 24 to 26 April 2023, I'll perform _Falling Down the Rabbit Hole_ together with my performance partner Esther Fuge at Gundeldinger Feld in Basel. For more information: [www.cirqueducercle.com](http://www.cirqueducercle.com)
</span>

<iframe src="https://player.vimeo.com/video/805703677?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Pascal Haering offers high-quality entertainment for any event. His show
acts can be tailered to your needs. With the experience of more than 600
performances in 12 years he will provide an exciting and memorable
experience for your guests.

<iframe src="https://player.vimeo.com/video/281065133?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

The Sumerians invented the wheel more than 5000 years ago and it quickly
turned into an indispensable tool for everyday life. Back then they
might not have guessed that it would also become a means for artistic
and acrobatic expression.

***„it really was a very beautiful and atmospheric performance"***\
(corporate client)

***„your act was more than excellent - specifically in connection with
the music it was dreamlike"***\
(audience member)

***"the audiences were very touched by your performance - the show was a
complete success - the feedback overwhelming"***\
(director of art on stage)

***„with the elegance of a dancer, accompanied by beautiful music"***\
(THERAPIE – Das Lifestylemagazin)

***„he creates impressive images on stage"***\
(Stadtspiegel Bochum)

---
title: Teaching
other-language: unterricht
colors:
  background: b50909
  blocks: 45ffbb
---

Teaching
========

### Bristol Cyr Wheel Weekends

Approximately every two months I am running a cyr wheel training weekend
in Bristol. Get in touch if you\'re interested.

### Workshops / Private Tuition

I have 20 years of teaching experience with kids and adults as circus,
ski and floorball instructor. Please contact me for workshops or private
tuition.

\
\
\
\
\
\
\
\
\

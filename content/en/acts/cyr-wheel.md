---
title: Cyr Wheel
other-language: show-acts/roue-cyr
colors:
  background: 333333
  blocks: a7d1c9
---

Cyr Wheel
=========

*"The Sumerians invented the wheel circa 3300 BC. The wheel was an
extraordinary invention that would change the course of human history.
Daniel Cyr, Cirque Éloize Co-Founder, designed the Cyr Wheel in 2003 AD.
The Cyr Wheel is as simple in shape as it is to use -- a device that
allows circus performers to execute a virtually infinite number of
acrobatic figures, each more complex and awe-inspiring than the last.
\[...\]*

*The Sumerians of yesteryear could not have foreseen the countless
possibilities that their invention has engendered. Daniel Cyr's wheel,
which is light and strong, is a device that is no less fabulous,
resembling a machine that may have sprung from the mind of Leonardo da
Vinci. Once learned, mastered, and used as an object of creation, the
Cyr Wheel becomes an extension of the body. As the Wheel turns, it also
turns the heads of awed spectators."*

From www.cirque-eloize.com

### Performance at "Switzerland's got Talent 2016"

<iframe src="https://player.vimeo.com/video/159610988?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Cirque du Circle

Hire for your event an exciting, inspiring and stunning performance with
a contemporary circus apparatus many of your guests won't know.
Pascal's act is highly skilled, elegant and simply mesmerising.

Duration: 5-6 minutes

Technical requirement:\
- Minimum 4.5m x 4.5m stage or performance space with a hard floor\
- PA system

Video (regular stage):\
<iframe src="https://player.vimeo.com/video/281103939?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Video (large stage):\
<iframe src="https://player.vimeo.com/video/178342547?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Bespoke performance, tailored to your event

Anything is possible: A cyr wheel and fire show, cyr wheel and live
music (e.g. cyr wheel and marimba), a choreography to your preferred
music, or an artistic show with animated projections and shadows.
Contact me with your own idea and I'll make it possible for your event.

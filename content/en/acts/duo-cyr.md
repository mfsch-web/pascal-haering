---
title: Duo Cyr
other-language: show-acts/duo-cyr
colors:
  background: 1c1c1c
  blocks: ad1a30
---

Duo Cyr Wheel
=============

### Swing Wheel

An impressive, entertaining and cheeky duo cyr wheel act, inspired by
swing dance or adapted to your event theme.

Duration: 5-7 minutes

Technical requirements:\
- Minimum stage size: 5 x 5m\
- PA system

Video:\
<iframe src="https://player.vimeo.com/video/354644912?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>\
Performed by Pascal Haering and Esther Fuge

### Duo Circus Show

Esther and I can offer up do 40 minutes of circus entertainment,
combining solo and duo cyr wheel, solo and duo juggling, hula hoop, cube
manipulation and aerial hoop. Please get in touch for further
information.

---
title: Cyrimba
other-language: show-acts/cyrimba
colors:
  background: 363636
  blocks: ebf7ff
---

Cyrimba
=======

Cyrimba is a collaboration between marimba player and composer Harriet
Riley and cyr wheel artist Pascal Häring. It is a fusion of contemporary
music and circus/movement choreography.

Our new act, available for corporate galas, functions and other events,
is a composition of original music and choreography, where the music and
movement inform each other and merge to a beautiful integral
performance.

<iframe src="https://player.vimeo.com/video/189149336?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

For bookings or further information, please contact <info@cyrimba.com>

![IMG_4886.jpg](/img/IMG_4886.jpg){width="575" height="324"}

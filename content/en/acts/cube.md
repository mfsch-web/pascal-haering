---
title: Cube
other-language: show-acts/cube
colors:
  background: 143291
  blocks: b0b0b0
---

Cube Manipulation
=================

A highly visual act, where Pascal Haering spins a 1.20-meter titanium
cube above his head and around his body. This awe-inspiring circus
discipline is rarely seen nowadays and is impressive, powerful and
enchanting at the same time. The perfect addition to a cyr wheel act.

Duration: 4 minutes

Technical requirements:\
- Unobstructed performance area with a diameter of 6m and 4.5m clear
height above the stage. (Die actual stage floor can be as small as 2m x
2m, but the clear \"air space\" must measure at least 6m in all
directions.)\
- PA system

Video:\
<iframe src="https://player.vimeo.com/video/380377323?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>\
Performed by Pascal Haering

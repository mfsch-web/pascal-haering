---
title: Juggling
other-language: show-acts/jonglage
colors:
  background: ff4400
  blocks: ffdd00
---

Juggling
========

Enjoy the perfect timing of juggling and music, combined with
captivating character work. Pascal's juggling acts can be customised
and performed in different style.

### Butterfly

Juggling act with 3 to 5 balls, perfectly choreographed to music.

Duration: 5:30min minutes, 3 to 5 juggling balls

Technical requirements:\
- Approx. 3m height\
- PA system

Video:\
<iframe src="https://player.vimeo.com/video/225145138?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Ball & Cane

Duration: Approx. 6-7 minutes

Technical requirements:\
- Approx. 4m height\
- PA system

Video:\
<iframe src="https://player.vimeo.com/video/46420339?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Passing

Duration: On request

Technical requirements:\
- Approx. 4m height (or more)\
- PA system

Video:\
<iframe src="https://player.vimeo.com/video/60703037?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### Choreography to your music

Send me your favourite piece of music and I'll create a jugging
choreography for your event.

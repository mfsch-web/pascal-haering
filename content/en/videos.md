---
title: Videos
other-language: videos
colors:
  background: a4d6c5
  blocks: 3d3d3d
  text: ffffff
---

Videos
======

(If you can't play the videos on this page, please click on the titel
of the video.)

### [Showreel 2019](https://vimeo.com/281065133)

<iframe src="https://player.vimeo.com/video/281065133?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artist: Pascal Haering

### [Duo Cyr Wheel Trailer](https://vimeo.com/354644912)

<iframe src="https://player.vimeo.com/video/354644912?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artists: Pascal Haering and Esther Fuge

### [Cube Trailer](https://vimeo.com/380377323)

<iframe src="https://player.vimeo.com/video/380377323?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artist: Pascal Haering

### [Cyr Wheel @ Corporate Event](https://vimeo.com/281103939)

<iframe src="https://player.vimeo.com/video/281103939?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Cyr wheel act "Cirque du Cercle" at a corporate event in February
2018.\
Artist: Pascal Häring

### [Juggling Act "Butterfly"](https://vimeo.com/225145138)

<iframe src="https://player.vimeo.com/video/225145138?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Artist: Pascal Häring

### [Showreel 2016](https://vimeo.com/158477407)

<iframe src="https://player.vimeo.com/video/158477407?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

### [Cyr Wheel @ Glastonbury Festival 2016](https://vimeo.com/178342547)

<iframe src="https://player.vimeo.com/video/178342547?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Cyr wheel act "Cirque du Cercle", performed at the Circus Big Top of
Glastonbury Festival 2016\
Artist: Pascal Häring

### [Performance @ Switzerland's got Talent 2016](https://www.youtube.com/watch?v=88biuv4lkpU)

<iframe  src="https://www.youtube.com/embed/88biuv4lkpU?showinfo=0" frameborder="0" width="575" height="323"></iframe>

### [Cyr Wheel @ Color Fantasy](https://vimeo.com/77131592)

<iframe src="https://player.vimeo.com/video/77131592?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

This act has been developed for and performed for the show "Pirates of
the Skagerrak" on boad the M/S Color Fantasy by Color Line, a ferry
cruising between Kiel (GER) and Oslo (N). Performed in October 2013.
Performer: Pascal Haering

### [Roue Cyr @ Swiss Christmas 2012](https://vimeo.com/55417250)

<iframe src="https://player.vimeo.com/video/55417250?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Cyr wheel act performed at the Swiss Christmas Show 2012: "Diamond
Illusion" in Zurich Oerlikon, 22 November - 31 December 2012,
Performer: Pascal Haering

### [Juggling Act with Ball and Cane](https://vimeo.com/46420339)

<iframe src="https://player.vimeo.com/video/46420339?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Performed by Pascal Haering, July 2012

### [Adagio Act: Lederhosen](https://vimeo.com/24064578)

<iframe src="https://player.vimeo.com/video/24064578?title=0&byline=0&portrait=0" frameborder="0" width="575" height="323"></iframe>

Performed by Pascal Haering and Jemadeekara Lewandowski-Porter at
Cabaret Vertigo in Melbourne, May 2011

All videos © by Pascal Haering, 2010-2016

---
title: Aug 2018
other-language: unterricht
colors:
  background: 491085
  blocks: 8fc0ff
---

![Facebook-Banner-July.jpg](/img/ac852ccb48aacaeecb53631d02987af4_f135.jpg){width="570" height="320"}

Bristol Cyr Wheel Intensive
===========================

### August 31st to September 2nd, 2018 (Friday - Sunday)

Come and join me for 3 days of wheel spinning fun. Whether you are a
pure beginner or fairly advanced doesn't matter, we'll work on your
level.

\- 9 hours of training in 3 days\
- open to all levels\
- large space (the hall is big enough so that everyone can spin at the
same time!)\
- A limited number of wheels are available. If you don\'t own a wheel,
get in touch with me\
- Additional unsupervised practice time

### Beginners class

9.30am - 1.30pm (with 1hr break at 11am)

### Intermediate / Advanced Class

2pm - 6pm (with 1 hr break at 3.30pm)

### Prices

£50 for one day\
£90 for two days\
£120 for all three days\
**EARLY BIRD: £105 for all 3 days if booked before 12 August**

### Bookings

By email to <cyrwheelbristol@gmail.com>. When signing up, please state
the following information:\
- Your name\
- Whether you own a wheel or you need to rent one\
- Your height - Your level (tell me whether you've never touched he
wheel, or what you've last been working at)\
- Your preferred class (beginners or intermediate/advanced).

I will get back to you if I think the other class would be more
appropriate for you.

### Payment

Once you've signed up and to confirm your booking, I'll ask you to pay
the full course fee. You can cancel up to three weeks before the course
to get a full refund.

### Course Content

Depending on your level we will cover\
- Basic waltz\
- Technique of centred spin, cartwheel and coin\
- Various tricks based on these basic moves\
- Wheel manipulation\
- Creative work to

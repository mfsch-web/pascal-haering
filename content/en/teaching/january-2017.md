---
title: Jan 2018
other-language: unterricht
colors:
  background: 000000
  blocks: ffdd00
---

![Facebook-Banner-Jan.jpg](/img/8485ae043333527b8a0e860d598a442f_f129.jpg){width="570" height="320"}

Bristol Cyr Wheel Intensive
===========================

### January 20th to 22nd (Saturday - Monday)

Come and join me for 3 days of wheel spinning fun. Whether you are a
pure beginner or fairly advanced doesn't matter, we'll work on your
level.

\- 9 hours of training in 3 days\
- open to all levels\
- large space (the hall is big enough so that everyone can spin at the
same time!)\
- A limited number of wheels are available. If you don't own a wheel,
get in touch with me\
- Additional unsupervised practice time

### Beginners class

9am - 10.30am and 11.30am - 1pm

### Intermediate / Advanced Class

2pm - 3.30pm and 4.30pm - 6pm

#### Prices

£50 for one day\
£90 for two days\
£120 for all three days\
**EARLY BIRD: £105 for all 3 days if booked before 20 December**

#### Bookings

By email to <cyrwheelbristol@gmail.com>. You will then be asked to pay
a £20 deposit to secure your space. When signing up, please state the
following information:\
- Your name\
- Whether you own a wheel or you need to rent one\
- Your height - Your level (tell me whether you've never touched he
wheel, or what you've last been working at)\
- Your preferred class (beginners or intermediate/advanced).

I will get back to you if I think the other class would be more
appropriate for you.

#### Course Content

Depending on your level we will cover\
- Basic waltz\
- Technique of centred spin, cartwheel and coin\
- Various tricks based on these basic moves\
- Wheel manipulation\
- Creative work to find your own styles and movements with the

---
title: Nov 2019
other-language: unterricht
colors:
  background: 000000
  blocks: ff61ff
---

![Facebook-Banner-November.jpg](/img/1071d75d129c23bfa18eaae65efb95b4_f148.jpg){width="580" height="326"}

Bristol Cyr Wheel Intensive
===========================

### Saturday - Sunday, November 16th & 17th, 2019

**TIME\
**Beginners: 10am - 2pm both days\
Intermediate/Advanced: 2pm - 6pm both days\
(all classes have a 1-hour break in the middle)

**VENUE** \
The Invisible Circus\
Unit 15\
Premier Business Park\
Sussex St\
Bristol\
BS2 0RA

Come and join me for a weekend of cyr wheel spinning fun. Whether you
are a pure beginner or fairly advanced doesn't matter, we'll work on
your level.

\- 6 hours of training in 2 days\
- open to all levels\
- large space (the hall is big enough so that everyone can spin at the
same time)\
- A limited number of wheels are available. If you don't own a wheel,
get in touch with me

**PRICES**\
£90 for two days\
(£50 for one day)

**BOOKINGS**\
By email to <cyrwheelbristol@gmail.com>. When signing up, please state
the following information:\
- Your name\
- Whether you own a wheel or you need to rent one\
- Your height\
- Your level (tell me whether you've never touched he wheel, or what
you've last been working at)\
- Your mobile phone number

**PAYMENT**\
Once you've signed up and to confirm your booking, I'll ask you to pay
the full course fee.

**COURSE CONTENT:**\
Depending on your level we will cover\
- Basic waltz\
- Technique of centred spin, cartwheel and coin\
- Various tricks based on these basic moves\
- Wheel manipulation\
- Creative work to find your own styles and movements with the wheel

**ACCOMMODATION**\
There is a possibility to hire cheap accommodation right next to the
training space (in converted shipping containers). Get in touch if
you're interested.

**TEACHER**\
I started training cyr wheel nine years ago at a circus school in New
Zealand and have since been performing with it all over the place. For
the last 3 years I have been running cyr wheel weekend courses in
Bristol and other places. If you want to know more, check out my
webpage:
[www.pascalhaering.com](/)

**CONTACT**\
Pascal Haering\
<cyrwheelbristol@gmail.com>\
07534400992
